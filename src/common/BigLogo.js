import React from "react";

export default function BigLogo() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="140.694"
      height="97.232"
      viewBox="0 0 140.694 97.232"
    >
      <g
        id="Group_3281"
        data-name="Group 3281"
        transform="translate(-117.75 -194)"
      >
        <g id="Group_3050" data-name="Group 3050">
          <path
            id="Path_3803"
            data-name="Path 3803"
            d="M.23,24.533a2.111,2.111,0,0,1,.619-1.492A2,2,0,0,1,2.4,22.422a1.942,1.942,0,0,1,1.492.619,2.027,2.027,0,0,1,.647,1.492V47.054a2,2,0,0,1-.647,1.464,1.942,1.942,0,0,1-1.492.591A2,2,0,0,1,.906,48.49a2.083,2.083,0,0,1-.619-1.464Z"
            transform="translate(118.77 240.815)"
            fill="#214c99"
            stroke="#224c97"
            stroke-width="2.5"
          />
          <path
            id="Path_3804"
            data-name="Path 3804"
            d="M10.989,29.245a9.149,9.149,0,0,0-2.815,6.7A8.671,8.671,0,0,0,10.989,42.5a9.149,9.149,0,0,0,6.559,2.815,9.684,9.684,0,0,0,6.447-2.2,2.083,2.083,0,0,1,1.548-.366,2.365,2.365,0,0,1,1.492.788,2.252,2.252,0,0,1-.366,3.068A14.076,14.076,0,0,1,17.829,49.6,13.6,13.6,0,0,1,8,45.573,13.344,13.344,0,0,1,8,26.2a13.738,13.738,0,0,1,9.825-4.054,13.85,13.85,0,0,1,8.811,2.984,2.28,2.28,0,0,1,.788,1.436,2.2,2.2,0,0,1-.422,1.6,2.055,2.055,0,0,1-1.492.816,1.858,1.858,0,0,1-1.548-.394,9.543,9.543,0,0,0-6.306-2.111,9.346,9.346,0,0,0-6.672,2.759Z"
            transform="translate(125.324 240.325)"
            fill="#214c99"
            stroke="#224c97"
            stroke-width="2.5"
          />
          <path
            id="Path_3805"
            data-name="Path 3805"
            d="M33.779,23.059a2.139,2.139,0,0,1-1.492,3.6H26.263V47.016a2.09,2.09,0,0,1-3.547,1.492,2,2,0,0,1-.619-1.492V26.663H16.072a2.055,2.055,0,0,1-1.492-.619,2.027,2.027,0,0,1-.619-1.52,1.942,1.942,0,0,1,.619-1.464,2.055,2.055,0,0,1,1.492-.619H32.343a1.942,1.942,0,0,1,1.436.619Z"
            transform="translate(143.691 240.853)"
            fill="#214c99"
            stroke="#224c97"
            stroke-width="2.5"
          />
          <path
            id="Path_3806"
            data-name="Path 3806"
            d="M25.554,49.116a2.027,2.027,0,0,1-2.083-2.083V24.512a2.027,2.027,0,0,1,.591-1.492,2,2,0,0,1,1.492-.619,2.111,2.111,0,0,1,1.52.619,1.942,1.942,0,0,1,.619,1.492V44.837H38a2.224,2.224,0,0,1,1.492.619,2.168,2.168,0,0,1,.619,1.492,2.111,2.111,0,0,1-.619,1.492A2.083,2.083,0,0,1,38,49.06Z"
            transform="translate(160.953 240.78)"
            fill="none"
            stroke="#224c97"
            stroke-width="2.5"
          />
          <path
            id="Path_3807"
            data-name="Path 3807"
            d="M31.19,24.525a1.942,1.942,0,0,1,.619-1.464,1.8,1.8,0,0,1,1.464-.619,1.971,1.971,0,0,1,1.52.619,1.942,1.942,0,0,1,.619,1.464V47.046a1.942,1.942,0,0,1-.619,1.464,2.055,2.055,0,0,1-1.52.619,1.886,1.886,0,0,1-1.464-.619,1.942,1.942,0,0,1-.619-1.464Z"
            transform="translate(174.966 240.851)"
            fill="none"
            stroke="#224c97"
            stroke-width="2.5"
          />
          <path
            id="Path_3808"
            data-name="Path 3808"
            d="M38.814,48.507a2.055,2.055,0,0,1-1.492.619,1.942,1.942,0,0,1-1.492-.619,1.971,1.971,0,0,1-.619-1.464V24.522a2,2,0,0,1,.619-1.492,1.942,1.942,0,0,1,1.492-.619H49.765a2.224,2.224,0,0,1,2.111,2.139,2.111,2.111,0,0,1-.647,1.492,2.083,2.083,0,0,1-1.464.619H39.433v6.981h8.9a2.055,2.055,0,0,1,1.464.591,2.111,2.111,0,0,1,.619,1.52,1.97,1.97,0,0,1-.619,1.464,1.914,1.914,0,0,1-1.464.647h-8.9v9.149a1.971,1.971,0,0,1-.619,1.492Z"
            transform="translate(182.263 240.798)"
            fill="none"
            stroke="#224c97"
            stroke-width="2.5"
          />
          <path
            id="Path_3809"
            data-name="Path 3809"
            d="M45.484,49.115a2.027,2.027,0,0,1-1.464-.591,2,2,0,0,1-.619-1.492V24.511A2.111,2.111,0,0,1,45.484,22.4H57.955a2.168,2.168,0,0,1,2.111,2.111A2.083,2.083,0,0,1,59.418,26a1.971,1.971,0,0,1-1.464.619H47.623V33.66h8.9a2.083,2.083,0,0,1,1.464.619,2.168,2.168,0,0,1,.591,1.492,2.027,2.027,0,0,1-.591,1.492,1.942,1.942,0,0,1-1.464.619h-8.9v7.01H57.955A2.165,2.165,0,0,1,60.066,47a2.027,2.027,0,0,1-.647,1.492,1.971,1.971,0,0,1-1.464.619Z"
            transform="translate(197.128 240.78)"
            fill="none"
            stroke="#224c97"
            stroke-width="2.5"
          />
        </g>
        <g
          id="Group_2809"
          data-name="Group 2809"
          transform="translate(165.562 194)"
        >
          <path
            id="Path_3810"
            data-name="Path 3810"
            d="M40.452,4.49V9.66a14.76,14.76,0,1,1-8.552,0V4.49a19.785,19.785,0,1,0,8.552,0Z"
            transform="translate(-16.389 6.357)"
            fill="#224c97"
          />
          <rect
            id="Rectangle_440"
            data-name="Rectangle 440"
            width="5.387"
            height="17.732"
            transform="translate(17.105 6.619)"
            fill="#224c97"
          />
          <rect
            id="Rectangle_441"
            data-name="Rectangle 441"
            width="2.198"
            height="0.797"
            transform="translate(18.699 34.884)"
            fill="#224c97"
          />
          <rect
            id="Rectangle_442"
            data-name="Rectangle 442"
            width="2.996"
            height="0.918"
            transform="translate(18.289 32.468)"
            fill="#224c97"
          />
          <rect
            id="Rectangle_443"
            data-name="Rectangle 443"
            width="3.793"
            height="1.015"
            transform="translate(17.878 30.052)"
            fill="#224c97"
          />
          <rect
            id="Rectangle_444"
            data-name="Rectangle 444"
            width="4.614"
            height="1.184"
            transform="translate(17.467 27.612)"
            fill="#224c97"
          />
          <rect
            id="Rectangle_445"
            data-name="Rectangle 445"
            width="5.387"
            height="1.28"
            transform="translate(17.105 25.221)"
            fill="#224c97"
          />
          <path
            id="Path_3811"
            data-name="Path 3811"
            d="M26.176,0,23.47,5.46h5.387Z"
            transform="translate(-6.365)"
            fill="#224c97"
          />
        </g>
      </g>
    </svg>
  );
}
