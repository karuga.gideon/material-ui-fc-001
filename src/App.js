import React from "react";
import { Switch, Route } from "react-router-dom";

import Login from "./components/Login";
import About from "./components/About";
import Contact from "./components/Contact";
import Customers from "./components/pages/Customers";
import Dashboard from "./components/pages/Dashboard";
import Home from "./components/Home";
import Integrations from "./components/pages/Integrations";
import MyOrders from "./components/pages/MyOrders";
import Reports from "./components/pages/Reports";

function App() {
  return (
    <Switch>
      <Route path="/" exact component={Dashboard} />
      <Route path="/login" component={Login} />
      <Route path="/about" component={About} />
      <Route path="/contact" component={Contact} />
      <Route path="/customers" component={Customers} />
      <Route path="/dashboard" component={Dashboard} />
      <Route path="/home" component={Home} />
      <Route path="/integrations" component={Integrations} />
      <Route path="/orders" component={MyOrders} />
      <Route path="/reports" component={Reports} />
    </Switch>
  );
}

export default App;
