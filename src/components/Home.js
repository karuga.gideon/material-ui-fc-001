import React from "react";
import Header from "../components/layouts/Header";
import SideMenu from "../components/navigation/SideMenu";

export default function Home() {
  return (
    <div>
      <Header />
      <SideMenu />
      <h1>Home Component - Here we are home.</h1>
    </div>
  );
}
