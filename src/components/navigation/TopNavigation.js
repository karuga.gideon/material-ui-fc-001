import React from "react";
import { NavLink } from "react-router-dom";

export default function TopNavigation() {
  return (
    <nav style={{ margin: 10 }}>
      <NavLink to="/" exact activeClassName="active">
        Home
      </NavLink>

      <NavLink to="/about" style={{ padding: 10 }}>
        About
      </NavLink>

      <NavLink to="/contact" style={{ padding: 10 }}>
        Contact
      </NavLink>

      <NavLink to="/login" style={{ padding: 10 }}>
        Login
      </NavLink>
    </nav>
  );
}
