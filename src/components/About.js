import React from "react";
import Header from "../components/layouts/Header";
import SideMenu from "../components/navigation/SideMenu";

export default function About() {
  return (
    <div>
      <Header />
      <SideMenu />
      <h1>
        About Component - This is a long line explaining the about details.
      </h1>
    </div>
  );
}
